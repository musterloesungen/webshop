import axios from 'axios';
import { useEffect, useState, useContext } from 'react';
import ProductCard from '../components/ProductCard';
import ShoppingCart from '../components/ShoppingCart';
import CartContext from '../context/CartContext';
import styles from '../styles/Home.module.css';

export default function Index() {
    const [products, setProducts] = useState([]);
    const { cartItems } = useContext(CartContext);

    useEffect(() => {
        axios.get('/api/products')
            .then(res => setProducts(res.data))
            .catch(err => console.error(err));
    }, []);

    return (
        <div className={styles.container}>
            <div className={styles.products}>
                {products.map(product => (
                    <ProductCard key={product.id} product={product} />
                ))}
            </div>
            <div className={styles.cart}>
                <ShoppingCart cartItems={cartItems} />
            </div>
        </div>
    );
}
