import '@/styles/globals.css'
import type { AppProps } from 'next/app'
import { CartProvider } from '../context/CartContext';
import { appWithTranslation } from 'next-i18next';

function App({ Component, pageProps }: AppProps) {
  return (
    <CartProvider>
      <Component {...pageProps} />
    </CartProvider>
  )
}

export default appWithTranslation(App);
