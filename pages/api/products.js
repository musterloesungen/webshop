const products = [
    { id: 1, name: 'Product A', description: 'Description of Product A', price: 10.99, image: '/images/product-a.png' },
    { id: 2, name: 'Product B', description: 'Description of Product B', price: 15.99, image: '/images/product-b.png' },
    { id: 3, name: 'Product C', description: 'Description of Product C', price: 12.99, image: '/images/product-c.png' }
];

export default function handler(req, res) {
    res.status(200).json(products);
}
