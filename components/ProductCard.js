import React, { useContext } from 'react';
import CartContext from '../context/CartContext';
import styles from '../styles/ProductCard.module.css';
import Image from 'next/image';
import { useTranslation } from 'next-i18next';

export default function ProductCard({ product }) {
    const { addToCart } = useContext(CartContext);
    const { i18n } = useTranslation();

    const formattedPrice = new Intl.NumberFormat(i18n.language, {
        style: 'currency',
        currency: 'EUR' // Setzen Sie hier Ihre Standardwährung ein oder passen Sie sie dynamisch an
    }).format(product.price);

    return (
        <div className={styles.card}>
            <h3>{product.name}</h3>
            <div className={styles.imageContainer}>
                <Image
                    src={product.image}
                    alt={product.name}
                    width={100} // Set desired width
                    height={100} // Set desired height
                    layout="responsive" // This keeps the aspect ratio
                />
            </div>
            <p>{product.description}</p>
            <p>{formattedPrice}</p>
            <button onClick={() => addToCart(product)}>Add to Cart</button>
        </div>
    );
}
