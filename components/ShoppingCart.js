import React, { useContext } from 'react';
import styles from '../styles/ShoppingCart.module.css';
import CartContext from '../context/CartContext';

export default function ShoppingCart() {
    const { cartItems, removeFromCart } = useContext(CartContext);

    return (
        <div className={styles.cartContainer}>
            <h2>Shopping Cart</h2>
            {cartItems.length > 0 ? (
                cartItems.map(item => (
                    <div key={item.id} className={styles.cartItem}>
                        <p>{item.name}</p>
                        <button onClick={() => removeFromCart(item.id)}>Remove</button>
                    </div>
                ))
            ) : (
                <p>Your cart is empty.</p>
            )}
        </div>
    );
}
